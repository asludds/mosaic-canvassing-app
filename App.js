import {createStackNavigator} from 'react-navigation';
import routeConfig from './src/routeConfig';

// export default createStackNavigator(routeConfig,{initialRouteName:"TurfScreen",headerMode : 'screens'});
export default createStackNavigator(routeConfig,{initialRouteName:"TurfScreen"});
