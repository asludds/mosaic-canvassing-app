import HomeScreen from './screens/home';
import VisitScreen from './screens/VisitScreen';
import ListView from './screens/listscreen';
import ScriptTab from './screens/scriptTab';
import {createMaterialTopTabNavigator} from 'react-navigation';
import TurfScreen from './screens/TurfScreen';
export default {
    MapTabs: createMaterialTopTabNavigator({
        Map : HomeScreen,
        List : ListView,
        Script : ScriptTab,
      },
      {
        tabBarPosition : 'bottom',
        headerMode : 'screens'
      }
    ),
    VisitScreen : {screen : VisitScreen},
    TurfScreen : {screen : TurfScreen},
}
