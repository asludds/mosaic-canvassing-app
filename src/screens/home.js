import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  StatusBar
} from 'react-native';
import {Container, Content} from 'native-base';

import MapViewDirections from 'react-native-maps-directions';
import MapView from 'react-native-maps';
import CustomMarkers from '../screens/CustomMarkers';

const URL = "http://18.217.134.19:5000/turfAddresses";

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      markers: [{"attributes":{"latitude":"0","longitude":"0"}}],
      userLatitude : 0,
      userLongitude : 0 
    }
    this.turfname = props.turfname;
    this.travelMethod = 'walking';
    this.generateMarkers = this.generateMarkers.bind(this);
  }

  //This removes the topbar from the screen so the map takes up the full screen
  // static navigationOptions = {
  //   header: null,
  // };

  async fetchMarkers(){
    try{
      const response = await fetch(URL + "?turfname=${this.turfname}",{method:'GET'});
      const responseJSON = await response.json();
      this.setState({markers : responseJSON.data});
     } catch(error){ 
        // console.warn(error);
    }
  }

  generateMapDirections(){
      return (
        <MapViewDirections origin={this.state.markers[0]}
            waypoints={this.state.markers.slice(1,this.state.markers.length-1)}
            language={"en"}
            mode={this.travelMethod}
            destination={this.state.markers[this.state.markers.length-1]}
            apikey={"AIzaSyAlyk_LH2341hT_nAkweRnPZHWd0eGToDs"}
            strokeWidth={3}
            strokeColor = "hotpink"
        />
      );
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
       (position) => {
         userLatitude: position.coords.latitude,
           this.setState({
           userLongitude: position.coords.longitude,
           error: null,
         });
       },
       (error) => this.setState({ error: error.message }),
       { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
     );
     this.fetchMarkers();
  }

  generateMarkers() { 
    return(
      this.state.markers.map((latlng, key) => 
        (
          <CustomMarkers markerkey = {key} latitude={latlng.attributes.latitude} longitude={latlng.attributes.longitude} navigation={this.props.navigation}/>
        ))
    );
  }
  
  render() {
    return (
            <MapView
                style={styles.container}
                toolbarEnabled = {false}
                pitchEnabled = {true}
                showsUserLocation = {true}
                showsMyLocationButton = {true}
                showsCompass = {true}
                showsBuildings = {true}
                showsScale = {true}
                pitchEnabled = {true}
                loadingEnabled = {true}
                initialRegion={{
                    latitude: 42.361600,
                    longitude: -71.098512,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }} 
            >
            {
              this.generateMarkers()
            }
            {
              this.generateMapDirections()
            }
        </MapView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  marker: {
    backgroundColor: "#550bbc",
    padding: 5,
    borderRadius: 5,
  },
  text: {
    color: "#FFF",
    fontWeight: "bold"
  }
});

AppRegistry.registerComponent('maps', () => maps);