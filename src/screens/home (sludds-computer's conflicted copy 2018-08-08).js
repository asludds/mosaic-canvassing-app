import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';
import {Container, Content} from 'native-base';

import MapViewDirections from 'react-native-maps-directions';
import MapView from 'react-native-maps';
import CustomMarkers from '../screens/CustomMarkers';
import SlidingUpPanel from 'rn-sliding-up-panel';

class MyComponent extends React.Component {
    state = {
      visible: false
    }
  
    render() {
      return (
        <View style={styles.container}>
          <Button title='Show panel' onPress={() => this.setState({visible: true})} />
          <SlidingUpPanel
            visible={this.state.visible}
            onRequestClose={() => this.setState({visible: false})}>
            <View style={styles.container}>
              <Text>Here is the content inside panel</Text>
              <Button title='Hide' onPress={() => this.setState({visible: false})} />
            </View>
          </SlidingUpPanel>
        </View>
      )
    }
  }



export default class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      markers: this.convertPropsToMarkers(props)
    }
    this.testNumbers = [{latitude:"45.5209087",longitude:"-122.6705107"},{latitude:"45.3209087",longitude:"-122.4705107"},{latitude:"45.7209087",longitude:"-122.8705107"}];
  }

  convertPropsToMarkers(props){
      return [];
  }

  generateMapDirections(){
      return (
        <MapViewDirections
            origin={this.testNumbers[0]}
            waypoints={this.testNumbers.slice(1,this.testNumbers.length-1)}
            language={"en"}
            mode={"walking"}
            destination={this.testNumbers[this.testNumbers.length-1]}
            apikey={"AIzaSyAlyk_LH2341hT_nAkweRnPZHWd0eGToDs"}
            strokeWidth={3}
            strokeColor = "hotpink"
        />
      );
  }

  generateMarkers(){
      return(
        this.testNumbers.map((latlng, key) => 
          (
              <CustomMarkers markerkey = {key} latitude={latlng.latitude} longitude={latlng.longitude}/>
          ))
    );
  }
  
  render() {
    return (
            <MapView 
                style={styles.container}
                toolbarEnabled = {false}
                pitchEnabled = {true}
                initialRegion={{
                    latitude: 45.5209087,
                    longitude: -122.6705107,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }} 
            >
            {
                this.generateMarkers()
                
            }
            {
                this.generateMapDirections()
            }
        </MapView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  marker: {
    backgroundColor: "#550bbc",
    padding: 5,
    borderRadius: 5,
  },
  text: {
    color: "#FFF",
    fontWeight: "bold"
  }
});

AppRegistry.registerComponent('maps', () => maps);