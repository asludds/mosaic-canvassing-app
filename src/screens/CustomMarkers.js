import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Button,
    TouchableOpacity,
  } from 'react-native';
import MapView from 'react-native-maps';
import styles from 'rn-sliding-up-panel/libs/styles';

export default class CustomMarkers extends Component{
    constructor(props){
        super(props);
        this.key = props.markerkey;
        this.latitude = parseFloat(props.latitude);
        this.longitude = parseFloat(props.longitude);
    }

    markerClick(){
    }

    popupContentButtonOnPress(){
        candidateInfo = "This is the info";
        this.props.navigation.navigate('VisitScreen',{
            candidateData : candidateInfo
        });
    }

    markerPopupContent(){
        return(
            <MapView.Callout width={210} onPress={() => {this.popupContentButtonOnPress()}}>
                    <Text>Text in opacity</Text>
            </MapView.Callout>
        );
    }

    render(){
        const { navigate } = this.props.navigation;
        return(
            <MapView.Marker 
                key = {this.key}
                coordinate={{latitude:this.latitude,longitude:this.longitude}}
                title={"Title"}
                description={"This is the description"}
                onPress={() => this.markerClick()}
            >
                {this.markerPopupContent()}
            </MapView.Marker>
        );
    }
}

const customMarkerStyles = StyleSheet.create({
    popupcontentview : {
        height : 100,
        width : 300,
    },
    marker: {
        backgroundColor: "#550bbc",
        padding: 5,
        borderRadius: 5,
    },
});

// const styles = StyleSheet.create({
//     container: {
//       flex: 1,
//     },
//     marker: {
//       backgroundColor: "#550bbc",
//       padding: 5,
//       borderRadius: 5,
//     },
//     text: {
//       color: "#FFF",
//       fontWeight: "bold"
//     }
//   });