import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  ScrollView,
} from 'react-native';

const URL = "http://18.188.111.82:5000/script";

export default class ScriptTab extends Component {
    constructor(){
        super();

        this.state = {script : ""};
    }

    componentDidMount(){
        this.fetchScriptFromServer().done();
    }

    async fetchScriptFromServer(){
        const response = await fetch(URL,{method : "GET"});
        const json = await response.json();
        const script = json.script;
        this.setState({script});
    }
    
    render(){
        return(
            <ScrollView style={styles.textView}>
                <Text style={styles.title}>Script</Text>
                <Text style={styles.text}>
                    {this.state.script}
                </Text>
            </ScrollView>
        );
    }
}


const styles = StyleSheet.create({
    textView : {
        backgroundColor : "white",
    },
    title : {
        textAlign : 'center',
        fontSize: 36,
        marginBottom: 10,
        fontWeight: 'bold',
        color: 'black'    
    },
    text: {
        fontSize: 24,
        color: 'black'
    },
});