import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';

export default class ListView extends Component {
    render(){
        return(
            <View style={styles.textView}>
                <Text style={styles.title}>This is text</Text>
                <Text></Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    textView : {
        backgroundColor : "white",
    },
    title : {
        textAlign : 'center',
        fontSize: 36,
        marginBottom: 10,
        fontWeight: 'bold',
        color: 'black'    
    },
    text: {
        fontSize: 24,
        color: 'black'
    },
});