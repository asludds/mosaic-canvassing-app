import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';


export default class VisitScreen extends Component {
    constructor(props) {
      super(props);

      this.candidateInfo = props.candidateInfo;
    }
    render(){
        const { navigate } = this.props.navigation;

        return(
            <View style={styles.container}>
                <Text style={styles.title}>VisitScreen</Text>
                <Text>{this.candidateInfo}</Text>
            </View>
            
        );
    }

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor : 'white',
    },
    title: {
      textAlign : 'center',
      color : 'black',
      fontSize : 30,

    },
    text: {
      color: "black",
      fontWeight: "bold"
    }
  });
