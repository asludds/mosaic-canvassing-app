import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';

const URLTurf = "http://18.217.134.19:5000/turfs";

export default class TurfScreen extends Component {
    constructor(props){
        super(props);

        this.state = {
            turfs : ["Currently there are no turfs available to walk"],
        }
    }

    async fetchTurf(){
        try{
          const response = await fetch(URLTurf,{method:'GET'});
          const responseJSON = await response.json();
          this.setState({turfs : responseJSON.data});
         } catch(error){ 
            // console.warn(error);
        }
      }
    
    componentDidMount(){
        this.fetchTurf();
    }

    generateTurfs() { 
        return(
          this.state.turfs.map((turf, key) => 
            (
              <Button title={turf} onPress={()=>this.buttonOnPress(turf)}></Button>
            ))
        );
      }

    buttonOnPress(turfname){
        this.props.navigation.navigate("MapTabs",{
            turfname : turfname,
        });

    }

    render(){
        return(
            <View>
                {this.generateTurfs()}
            </View>
        );
    }
}