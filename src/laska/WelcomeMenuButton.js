import React, { Fragment } from "react";
// eslint-disable-next-line no-unused-vars
import API from "./_laska_/API.js";
// eslint-disable-next-line no-unused-vars
import globals from "./_laska_/globals.js";
import withNavigationProp from "./_laska_/withNavigationProp.js";
import { Text, StyleSheet, TouchableHighlight } from "react-native";

const styles = StyleSheet.create({
  s781bb721: {
    color: `rgba(255, 255, 255, 1)`,
    fontSize: 40,
    fontWeight: `bold`
  },
  scd3cbd31: {
    alignItems: `center`,
    borderRadius: 15,
    justifyContent: `center`,
    paddingBottom: 5,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5
  }
});

class WelcomeMenuButton extends React.PureComponent {
  static navigationOptions = { title: "WelcomeMenuButton" };
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    // eslint-disable-next-line no-unused-vars
    const { props, state } = this;
    // eslint-disable-next-line no-unused-vars
    const helpers = this;
    // eslint-disable-next-line no-unused-vars
    const setState = this.setState.bind(this);
    // eslint-disable-next-line no-unused-vars
    const { navigate, goBack } = props.navigation || {};

    return (
      <Fragment>
        <TouchableHighlight
          disabled={props.disabled}
          onPress={props.onPress}
          style={[
            styles.scd3cbd31,
            {
              backgroundColor: props.disabled
                ? `rgba(50, 50, 50, 1)`
                : `rgba(204, 50, 76, 1)`,
              opacity: props.disabled ? 0.5 : 1
            }
          ]}
        >
          <Text style={styles.s781bb721}>Continue</Text>
        </TouchableHighlight>
      </Fragment>
    );
  }
}

WelcomeMenuButton.defaultProps = {
  onPress: () => {}
};

const AssertNavigationProp = withNavigationProp(WelcomeMenuButton);

export default AssertNavigationProp;

export { styles };
