import React, { Fragment } from "react";
// eslint-disable-next-line no-unused-vars
import API from "./_laska_/API.js";
// eslint-disable-next-line no-unused-vars
import globals from "./_laska_/globals.js";
import withNavigationProp from "./_laska_/withNavigationProp.js";
import { Text, StyleSheet, View } from "react-native";

const styles = StyleSheet.create({
  s95fee850: {
    color: `rgba(255, 255, 255, 1)`,
    fontSize: 30,
    textAlign: `center`
  },
  s215f44e1: {
    backgroundColor: `rgba(57, 66, 99, 1)`,
    flex: 1,
    maxHeight: 50,
    minHeight: 50
  },
  s3ce65bc2: { flex: 1 }
});

class VisitScreenVoter extends React.PureComponent {
  static navigationOptions = { title: "VisitScreenVoter" };
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    // eslint-disable-next-line no-unused-vars
    const { props, state } = this;
    // eslint-disable-next-line no-unused-vars
    const helpers = this;
    // eslint-disable-next-line no-unused-vars
    const setState = this.setState.bind(this);
    // eslint-disable-next-line no-unused-vars
    const { navigate, goBack } = props.navigation || {};

    return (
      <Fragment>
        <View style={styles.s3ce65bc2}>
          <View style={styles.s215f44e1}>
            <Text style={styles.s95fee850}>Text string</Text>
          </View>
        </View>
      </Fragment>
    );
  }
}

const AssertNavigationProp = withNavigationProp(VisitScreenVoter);

export default AssertNavigationProp;

export { styles };
