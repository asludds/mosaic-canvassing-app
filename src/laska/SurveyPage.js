import React, { Fragment } from "react";
// eslint-disable-next-line no-unused-vars
import API from "./_laska_/API.js";
// eslint-disable-next-line no-unused-vars
import globals from "./_laska_/globals.js";
import withNavigationProp from "./_laska_/withNavigationProp.js";
import { Text, StyleSheet, View } from "react-native";

const styles = StyleSheet.create({
  s1bda4ed1: {
    color: `rgba(255, 255, 255, 1)`,
    fontSize: 24,
    fontWeight: `bold`,
    height: 40,
    textAlign: `center`
  },
  sfa360602: { borderRadius: 20, flex: 1, maxHeight: 40 },
  sfa6f3a1e: { flex: 1 },
  s3ea4b7ef: { flex: 1 },
  s56dedfb5: { backgroundColor: `rgba(57, 66, 99, 1)`, flex: 1 }
});

class SurveyPage extends React.PureComponent {
  static navigationOptions = { title: "SurveyPage" };
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    // eslint-disable-next-line no-unused-vars
    const { props, state } = this;
    // eslint-disable-next-line no-unused-vars
    const helpers = this;
    // eslint-disable-next-line no-unused-vars
    const setState = this.setState.bind(this);
    // eslint-disable-next-line no-unused-vars
    const { navigate, goBack } = props.navigation || {};

    return (
      <Fragment>
        <View style={styles.s56dedfb5}>
          <View
            style={[
              styles.sfa360602,
              {
                backgroundColor: props.disabled
                  ? `rgba(50, 50, 50, 1)`
                  : `rgba(204, 50, 76, 1)`
              }
            ]}
          >
            <Text style={styles.s1bda4ed1}>Visits</Text>
          </View>
          <View style={styles.sfa6f3a1e} />
          <View style={styles.s3ea4b7ef} />
        </View>
      </Fragment>
    );
  }
}

const AssertNavigationProp = withNavigationProp(SurveyPage);

export default AssertNavigationProp;

export { styles };
