import React, { Fragment } from "react";
// eslint-disable-next-line no-unused-vars
import API from "./_laska_/API.js";
// eslint-disable-next-line no-unused-vars
import globals from "./_laska_/globals.js";
import withNavigationProp from "./_laska_/withNavigationProp.js";
import { Text, View, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  s9d2bd39c: { flex: 1 },
  s40c5a2bf: { flex: 1, flexDirection: `row` }
});

class CandidateListing extends React.PureComponent {
  static navigationOptions = { title: "CandidateListing" };
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    // eslint-disable-next-line no-unused-vars
    const { props, state } = this;
    // eslint-disable-next-line no-unused-vars
    const helpers = this;
    // eslint-disable-next-line no-unused-vars
    const setState = this.setState.bind(this);
    // eslint-disable-next-line no-unused-vars
    const { navigate, goBack } = props.navigation || {};

    return (
      <Fragment>
        <View onLayout={() => {}} style={styles.s40c5a2bf}>
          <View style={styles.s9d2bd39c}>
            <Text>Text string</Text>
          </View>
        </View>
      </Fragment>
    );
  }
}

const AssertNavigationProp = withNavigationProp(CandidateListing);

export default AssertNavigationProp;

export { styles };
