import React from "react";
import { View, SafeAreaView, Platform } from "react-native";
import Expo from "expo";

let Component = SafeAreaView;

if (Platform.OS === "android") {
  Component = props => (
    <View
      {...props}
      style={{ ...props.style, paddingTop: Expo.Constants.statusBarHeight }}
    >
      {props.children}
    </View>
  );
}

export default Component;
