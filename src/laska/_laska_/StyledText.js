import styled from "styled-components";

export default styled.div`
  a {
    ${props => props.linkColor && `color: ${props.linkColor};`} ${({
      textDecoration = "none"
    }) => `text-decoration: ${textDecoration};`};
  }

  ${props =>
    props.linkHoverColor &&
    `
    a:hover {
      color: ${props.linkHoverColor};
    }
  `};
`;
