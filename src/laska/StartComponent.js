import Expo from "expo";
import StartComponent from "./Main.js";

console.disableYellowBox = true;

Expo.registerRootComponent(StartComponent);
