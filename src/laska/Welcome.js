import React, { Fragment } from "react";
// eslint-disable-next-line no-unused-vars
import API from "./_laska_/API.js";
// eslint-disable-next-line no-unused-vars
import globals from "./_laska_/globals.js";
import withNavigationProp from "./_laska_/withNavigationProp.js";
import Icon from "./_laska_/Icon";
import { StyleSheet, Text, View, KeyboardAvoidingView } from "react-native";
import WelcomeMenuButton from "./WelcomeMenuButton.js";

const styles = StyleSheet.create({
  s7841f387: { color: `rgba(176, 182, 210, 1)`, fontSize: 70, left: 30 },
  sade60a33: {
    bottom: 40,
    color: `rgba(176, 182, 210, 1)`,
    fontSize: 34,
    left: 75
  },
  sce1bb23a: { color: `rgba(176, 182, 210, 1)`, fontSize: 34, right: 60 },
  s83d646ce: {
    alignItems: `flex-end`,
    flex: 1,
    justifyContent: `center`,
    flexDirection: `row`
  },
  s1f683b55: {
    color: `rgba(228, 232, 249, 1)`,
    fontSize: 26,
    marginBottom: 30,
    marginTop: 30,
    textAlign: `center`
  },
  s2a0d0b8a: { flex: 1 },
  sd70d5cba: { backgroundColor: `rgba(57, 66, 99, 1)`, flex: 1 }
});

class Welcome extends React.PureComponent {
  static navigationOptions = { title: "Welcome" };
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    // eslint-disable-next-line no-unused-vars
    const { props, state } = this;
    // eslint-disable-next-line no-unused-vars
    const helpers = this;
    // eslint-disable-next-line no-unused-vars
    const setState = this.setState.bind(this);
    // eslint-disable-next-line no-unused-vars
    const { navigate, goBack } = props.navigation || {};

    return (
      <Fragment>
        <KeyboardAvoidingView style={styles.sd70d5cba}>
          <View style={styles.s83d646ce}>
            <Icon
              iconIdentifier={`FontAwesome/flag`}
              style={styles.s7841f387}
            />
            <Text style={styles.sade60a33}>Mosaic</Text>
            <Text style={styles.sce1bb23a}>Canvassing</Text>
          </View>
          <View style={styles.s2a0d0b8a}>
            <Fragment>
              <Text style={styles.s1f683b55}>Text string</Text>
              <WelcomeMenuButton
                onPress={() => {
                  const request = async () => {
                    const response = await fetch(
                      `http://18.217.134.19:6969/candidates`,
                      {
                        method: "GET",
                        headers: {
                          Accept: "application/json",
                          "Content-Type": "application/json"
                        }
                      }
                    );
                    const json = await response.json();
                    var result = [];
                    var names = [];
                    for (var i in result) result.push([i, result[i]]);
                    names.push([i, result[i][0]]);
                    candidateInfo = { result: result, names: names };
                    navigate("candidate-list-page", {
                      candidateData: candidateInfo
                    });
                  };

                  request();
                }}
              />
            </Fragment>
          </View>
        </KeyboardAvoidingView>
      </Fragment>
    );
  }
}

const AssertNavigationProp = withNavigationProp(Welcome);

export default AssertNavigationProp;

export { styles };
