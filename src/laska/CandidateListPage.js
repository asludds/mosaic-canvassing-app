import React, { Fragment } from "react";
// eslint-disable-next-line no-unused-vars
import API from "./_laska_/API.js";
// eslint-disable-next-line no-unused-vars
import globals from "./_laska_/globals.js";
import withNavigationProp from "./_laska_/withNavigationProp.js";
import { Text, StyleSheet, ScrollView, View } from "react-native";
import CandidateListing from "./CandidateListing.js";

const styles = StyleSheet.create({
  s7b0030f9: {
    color: `rgba(255, 255, 255, 1)`,
    fontSize: 24,
    marginTop: 5,
    textAlign: `center`
  },
  sce59a20f: {
    color: `rgba(255, 255, 255, 1)`,
    fontSize: 24,
    textAlign: `center`
  },
  s5b594c36: { backgroundColor: `rgba(57, 66, 99, 1)`, flex: 1 }
});

class CandidateListPage extends React.PureComponent {
  static navigationOptions = { title: "CandidateListPage" };
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    // eslint-disable-next-line no-unused-vars
    const { props, state } = this;
    // eslint-disable-next-line no-unused-vars
    const helpers = this;
    // eslint-disable-next-line no-unused-vars
    const setState = this.setState.bind(this);
    // eslint-disable-next-line no-unused-vars
    const { navigate, goBack } = props.navigation || {};

    return (
      <Fragment>
        <View
          onLayout={() => {
            console.error(props.candidateData);
          }}
          style={styles.s5b594c36}
        >
          <Text style={styles.s7b0030f9}>Available to Canvass</Text>
          <ScrollView onLayout={() => {}}>
            <CandidateListing />
          </ScrollView>
          <Text style={styles.sce59a20f}>Login Optional</Text>
          <ScrollView />
        </View>
      </Fragment>
    );
  }
}

const AssertNavigationProp = withNavigationProp(CandidateListPage);

export default AssertNavigationProp;

export { styles };
