import React, { Fragment } from "react";
// eslint-disable-next-line no-unused-vars
import API from "./_laska_/API.js";
// eslint-disable-next-line no-unused-vars
import globals from "./_laska_/globals.js";
import withNavigationProp from "./_laska_/withNavigationProp.js";
import SurveyPage from "./SurveyPage.js";
import Welcome from "./Welcome.js";
import CandidateListPage from "./CandidateListPage.js";
import { StackNavigator } from "react-navigation";

const StackNavigatore4622ef0 = StackNavigator(
  {
    "survey-page": {
      screen: SurveyPage
    },
    welcome: {
      screen: Welcome
    },
    "candidate-list-page": {
      screen: CandidateListPage
    }
  },
  {
    navigationOptions: {}
  }
);

class Main extends React.PureComponent {
  static navigationOptions = { title: "Main" };
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    // eslint-disable-next-line no-unused-vars
    const { props, state } = this;
    // eslint-disable-next-line no-unused-vars
    const helpers = this;
    // eslint-disable-next-line no-unused-vars
    const setState = this.setState.bind(this);
    // eslint-disable-next-line no-unused-vars
    const { navigate, goBack } = props.navigation || {};

    return (
      <Fragment>
        <StackNavigatore4622ef0 />
      </Fragment>
    );
  }
}

const AssertNavigationProp = withNavigationProp(Main);

export default AssertNavigationProp;
